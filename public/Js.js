$(function(){
$('.multiple_sliders').slick({
    slidesToShow: 4, //сколько слайдов показывать в карусели
    slidesToScroll: 4, // сколько слайдов прокручивать за раз
    dots:true,
    infinite: true,
    arrows:true,
    });
$(".multiple_sliders").on('afterChange', function(event, slick, currentSlide){
    $("#page").text(currentSlide/4 + 1);
    });
});
